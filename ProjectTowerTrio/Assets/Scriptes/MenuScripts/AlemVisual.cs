using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AlemVisual : MonoBehaviour
{
    // Levels
    [SerializeField] private Image _itemLevelOne;
    [SerializeField] private Image _itemLevelTwo;
    [SerializeField] private Image _itemLevelThree;
    [SerializeField] private Image _itemLevelFore;
    [SerializeField] private Image _itemLevelFive;

    // Improvement (���������)
    [SerializeField] private Image _itemImprovementRayLevelTwo;
    [SerializeField] private Image _itemImprovementRayLevelThree;
    [SerializeField] private Image _itemImprovementRayLevelFore;
    [SerializeField] private Image _itemImprovementRayLevelFive;
    [SerializeField] private Image _itemImprovementGunTrioLevelTwo;
    [SerializeField] private Image _itemImprovementGunTrioLevelThree;
    [SerializeField] private Image _itemImprovementGunTrioLevelFore;
    [SerializeField] private Image _itemImprovementGunTrioLevelFive;

    private void Awake()
    {
        // ����������
    }
}
